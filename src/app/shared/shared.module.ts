import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [LoginComponent, NavbarComponent, FooterComponent],
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    RouterModule,
  ],
  exports: [NavbarComponent, FooterComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
