import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from '../shared/shared.module';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { InicioComponent } from './inicio/inicio.component';
import { HeroesComponent } from './heroes/heroes.component';
import { DragonComponent } from './dragon/dragon.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GraficaComponent } from './chart/grafica/grafica.component';
import { DatachartComponent } from './chart/datachart/datachart.component';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
import { MiscelaneaComponent } from './miscelanea/miscelanea.component';
import { CssComponent } from './miscelanea/css/css.component'

@NgModule({
  declarations: [
    PagesComponent,
    BienvenidoComponent,
    InicioComponent,
    HeroesComponent,
    DragonComponent,
    GraficaComponent,
    DatachartComponent,
    MiscelaneaComponent,
    CssComponent],
  imports: [
    MDBBootstrapModule.forRoot(),
    RouterModule,
    SharedModule,
    Ng2SearchPipeModule,
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ChartsModule, WavesModule,
  ],
  exports: [
    BienvenidoComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
