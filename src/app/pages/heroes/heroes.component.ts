import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/services.index';
import { HeroeModel } from '../../model/heroe.model';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  Heroes: Array<HeroeModel>
  constructor(
    private _HeroesSrv: HeroesService,
  ) {
    this.Heroes = this._HeroesSrv.getHeroes();
  }

  ngOnInit() {
    console.log(this.Heroes);
  }

}
