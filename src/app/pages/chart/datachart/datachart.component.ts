import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-datachart',
  templateUrl: './datachart.component.html',
  styleUrls: ['./datachart.component.scss']
})
export class DatachartComponent implements OnInit {
  public chartType: string = 'bar';

  public chartDatasets: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  tiposgraficas: { id: number; descripcion: string; tipo: string; }[];
  total: any;
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  constructor() { }

  ngOnInit() {
    this.tiposgraficas = [
      { id: 1, descripcion: 'Pastel', tipo: 'pie' },
      { id: 2, descripcion: 'Dona', tipo: 'doughnut' },
      { id: 3, descripcion: 'Barras', tipo: 'bar' },
      { id: 4, descripcion: 'Barras horizontal', tipo: 'horizontalBar' },
      { id: 5, descripcion: 'Polar', tipo: 'polarArea' }

    ]
  }
  setNewGraficas(tipo: any): void {
    // console.log(tipo);
    this.chartType = tipo;
  }
  graficas(event: any) {
    console.log(event);

    // camnbio grafica pastel
    this.chartDatasets[0].data = event.cantidad;
    this.total = event.cantidad.reduce(function (a, b) { return a + b; }, 0);
    this.chartLabels = event.tipo;
    // fin


  }

}
