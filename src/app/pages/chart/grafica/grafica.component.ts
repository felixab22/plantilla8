import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.scss']
})
export class GraficaComponent implements OnInit {
  @Output() cambioValor: EventEmitter<any> = new EventEmitter();
  menus: any;
  respuesta: any = { cantidad: null, tipo: null };
  constructor(
    @Inject(DOCUMENT) private _document,
  ) {
    this.menus = [
      { id: 1, tipo: 'Sexo' },
      { id: 2, tipo: 'Ubicación' },
      { id: 3, tipo: 'Nivel Educación' },
      { id: 4, tipo: 'Tipo de material domestico' }
    ]
  }

  ngOnInit() {
  }
  emitValor(item: any, link: any) {
    const selectores: any = this._document.getElementsByClassName('selector');
    for (const ref of selectores) {
      ref.classList.remove('colorAzul');
    }
    link.classList.add('colorAzul');

    if (item.id === 1) {
      this.respuesta.cantidad = [15, 59];
      this.respuesta.tipo = ['Femenino', 'masculino'];
      this.cambioValor.emit(this.respuesta);
    }
    if (item.id === 2) {
      this.respuesta.cantidad = [15, 59];
      this.respuesta.tipo = ['Rural', 'Urbano'];
      this.cambioValor.emit(this.respuesta);
    }
    if (item.id === 3) {
      this.respuesta.cantidad = [15, 59, 12, 14, 3, 4];
      this.respuesta.tipo = ['Sin estudios', 'Inicial', 'Primaria', 'Secundaria', 'Universitario', 'instituto'];
      this.cambioValor.emit(this.respuesta);
    }

  }
}
