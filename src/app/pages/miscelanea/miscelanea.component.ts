import { Component, OnInit } from '@angular/core';
import { AlumnosService } from 'src/app/services/services.index';

@Component({
  selector: 'app-miscelanea',
  templateUrl: './miscelanea.component.html',
  styleUrls: ['./miscelanea.component.scss']
})
export class MiscelaneaComponent implements OnInit {
  size = 10;
  listaAlumnos: any;

  loading = false;

  constructor(
    private _AlumnoSrv: AlumnosService
  ) {
    this.listaAlumnos = this._AlumnoSrv.notas;
  }

  ngOnInit() {

  }
  ejecutar() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000)
    console.log(this.loading)
  }
}
