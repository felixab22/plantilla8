import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/services.index';
import { DragonBallModel } from '../../model/dragon.model';

@Component({
  selector: 'app-dragon',
  templateUrl: './dragon.component.html',
  styleUrls: ['./dragon.component.scss']
})
export class DragonComponent implements OnInit {
  dragones: Array<DragonBallModel>;
  model: DragonBallModel;
  term = ''
  constructor(
    private _dragonSrv: HeroesService
  ) {
    this.model = new DragonBallModel();
    this.dragones = this._dragonSrv.getDragonBall()
  }
  ngOnInit() {
  }
  Information(dragon, basicModal) {
    console.log(dragon);
    this.model = dragon;
    basicModal.show();
  }

}
