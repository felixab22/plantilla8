import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  inicio: any;
  mostrar = true;
  personas: any;
  constructor() {
    this.inicio = { titulo: 'Maria Manrrique', description: 'Desarrollo de software en Angular' };
    this.personas = [{ nombre: 'Felix' }, { nombre: 'Jose' }, { nombre: 'Karol' }, { nombre: 'Helio' }]
  }

  ngOnInit() {
  }

}
