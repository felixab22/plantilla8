import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './pages/bienvenido/bienvenido.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { HeroesComponent } from './pages/heroes/heroes.component';
import { DragonComponent } from './pages/dragon/dragon.component';
import { DatachartComponent } from './pages/chart/datachart/datachart.component';
import { MiscelaneaComponent } from './pages/miscelanea/miscelanea.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: LoginComponent },
  {
    path: 'Plantilla',
    component: PagesComponent,
    children:
      [
        { path: 'bienvenido', component: BienvenidoComponent },
        { path: 'miscelaneas', component: MiscelaneaComponent },
        { path: 'inicio', component: InicioComponent },
        { path: 'heroes', component: HeroesComponent },
        { path: 'graficas', component: DatachartComponent },
        { path: 'dragon-ball', component: DragonComponent },
        // { path: '**', component: NopageComponent }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
