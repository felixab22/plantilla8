export class HeroeModel {
    idheroe: number;
    nombre: string;
    bio: string;
    img: string;
    aparicion: string;
    casa: string;
    idx?: number;
};
