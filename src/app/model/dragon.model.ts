export class DragonBallModel {
    iddragon: number;
    nombre: string;
    description: string;
    img: string;
    aparicion: string;
    nivel?: string;
}