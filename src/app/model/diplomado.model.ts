export class DiplmadoModel {
    public id_diplo: number;
    public section: string;
    public maestria: string;
    public asignatura: string;
    public sicle: string;
    public creditos: number;
}