import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AlumnosService {

    public notas = [
        { id: 1, nombre: 'Jose', nota1: 19, nota2: 10, nota3: 15, nota4: 11 },
        { id: 2, nombre: 'Juan', nota1: 11, nota2: 11, nota3: 18, nota4: 12 },
        { id: 3, nombre: 'Carmen', nota1: 12, nota2: 20, nota3: 16, nota4: 13 },
        { id: 4, nombre: 'Gabriel', nota1: 13, nota2: 11, nota3: 15, nota4: 8 },
        { id: 5, nombre: 'Hector', nota1: 14, nota2: 12, nota3: 16, nota4: 9 },
        { id: 6, nombre: 'Helio', nota1: 15, nota2: 13, nota3: 17, nota4: 8 },
        { id: 7, nombre: 'Julio', nota1: 16, nota2: 14, nota3: 11, nota4: 5 }
    ];

    constructor() { }
}
