import { Injectable } from '@angular/core';
import { HeroeModel } from '../model/heroe.model';
import { DragonBallModel } from '../model/dragon.model';

@Injectable({
    providedIn: 'root'
})
export class HeroesService {
    constructor() {

    }
    private heroes: HeroeModel[] = [
        {
            idheroe: 1,
            nombre: "Aquaman",
            bio: "El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.",
            img: "assets/img/hero/aquaman.png",
            aparicion: "1941-11-01",
            casa: "DC"
        },
        {
            idheroe: 2,
            nombre: "Batman",
            bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
            img: "assets/img/hero/batman.png",
            aparicion: "1939-05-01",
            casa: "DC"
        },
        {
            idheroe: 3,
            nombre: "Daredevil",
            bio: "Al haber perdido la vista, los cuatro sentidos restantes de Daredevil fueron aumentados por la radiación a niveles superhumanos, en el accidente que tuvo cuando era niño. A pesar de su ceguera, puede \"ver\" a través de un \"sexto sentido\" que le sirve como un radar similar al de los murciélagos.",
            img: "assets/img/hero/daredevil.png",
            aparicion: "1964-01-01",
            casa: "Marvel"
        },
        {
            idheroe: 4,
            nombre: "Hulk",
            bio: "Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).",
            img: "assets/img/hero/hulk.png",
            aparicion: "1962-05-01",
            casa: "Marvel"
        },
        {
            idheroe: 5,
            nombre: "Linterna Verde",
            bio: "Poseedor del anillo de poder que posee la capacidad de crear manifestaciones de luz sólida mediante la utilización del pensamiento. Es alimentado por la Llama Verde (revisada por escritores posteriores como un poder místico llamado Starheart), una llama mágica contenida en dentro de un orbe (el orbe era en realidad un meteorito verde de metal que cayó a la Tierra, el cual encontró un fabricante de lámparas llamado Chang)",
            img: "assets/img/hero/linterna-verde.png",
            aparicion: "1940-06-01",
            casa: "DC"
        },
        {
            idheroe: 6,
            nombre: "Spider-Man",
            bio: "Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.",
            img: "assets/img/hero/spiderman.png",
            aparicion: "1962-08-01",
            casa: "Marvel"
        },
        {
            idheroe: 7,
            nombre: "Wolverine",
            bio: "En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
            img: "assets/img/hero/wolverine.png",
            aparicion: "1974-11-01",
            casa: "Marvel"
        }
    ];
    private dragon: DragonBallModel[] = [
        {
            iddragon: 1,
            nombre: "Gogeta SS4",
            description: "Es el resultado de la fusión Saiyan nacida mediante la unión entre Goku y Vegeta realizando la Danza de la Fusión de los metamoranos, existiendo como la contraparte de Vegetto",
            img: "assets/img/dragon/gogetass4.png",
            aparicion: "2005-08-10",
            nivel: 'cuatro'
        },
        {
            iddragon: 2,
            nombre: "Gotenks",
            description: "Parece que Gotenks solo tiene los peores rasgos de la personalidad de Goten y Trunks. Es egoísta, grosero, presumido, confía demasiado en sus habilidades y enfada a Piccolo con sus payasadas. Sin embargo, los increíbles poderes de Gotenks compensan su mala actitud. En su primera aparición, Gotenks se apresuró a desafiar inmediatamente a Majin Buu, recibiendo una tremenda paliza.",
            img: "assets/img/dragon/gotenks.png",
            aparicion: "2001-04-01",
            nivel: 'cero'
        },
        {
            iddragon: 3,
            nombre: "Vegeta SS4",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/vegetass4.png",
            aparicion: "2007-12-21",
            nivel: 'cuatro'
        },
        {
            iddragon: 4,
            nombre: "Chaozu",
            description: "Chaozu está basado en vampiros chinos, los Jiang Shi. Hay varias similitudes, como en la que ambos pasan la mayoría del tiempo volando, los Jiang Shi siempre tienen sus brazos extendidos como hace Chaoz al realizar varios de sus ataques, son de piel blanca y mejillas rojas, además llevan ropa Quing.",
            img: "assets/img/dragon/chaozu.png",
            aparicion: "1992-05-11",
            nivel: 'cero'
        },
        {
            iddragon: 5,
            nombre: "Guku SS2",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/gokuss2.png",
            aparicion: "2007-12-21",
            nivel: 'dos'
        },
        {
            iddragon: 6,
            nombre: "Trunks",
            description: "Es un mestizo entre humano terrícola y Saiyan nacido en la Tierra, e hijo de Bulma y Vegeta, el cual es introducido en la Saga de Cell. Más tarde en su vida como joven, se termina convirtiendo en un luchador de artes marciales, el mejor amigo de Son Goten y en el hermano mayor de su hermana Bra.",
            img: "assets/img/dragon/trunks.png",
            aparicion: "2004-03-22",
            nivel: 'cero'
        },
        {
            iddragon: 10,
            nombre: "Vegeta",
            description: "Es el deuteragonista de Dragon Ball Z, Dragon Ball Z Kai, Dragon Ball GT y Dragon Ball Super. Es el hijo mayor del Rey Vegeta III, así como el príncipe más reciente de la Familia Real Saiyan y uno de los pocos superviviente tras el Genocidio de los Saiyan del Planeta Vegeta del Universo 7, destruido a manos de Freezer.",
            img: "assets/img/dragon/vegeta.png",
            aparicion: "2003-11-12",
            nivel: 'cero'
        },
        {
            iddragon: 17,
            nombre: "Vegetto",
            description: "También conocido como Vegetto en Hispanoamérica, Vegito, Vegeku en España, o Vegerot, es el resultado de la unión entre Vegeta y Kacarrot mediante los Pendientes Pothala dados por las deidades Kaio-shin, el cual aparece en la Saga de Majin Boo y la Saga de Trunks del Futuro y existe como la contraparte de Gogeta, su fusión por la Danza metamorana.",
            img: "assets/img/dragon/vegetto.png",
            aparicion: "2011-04-11",
            nivel: 'cero'
        },
        {
            iddragon: 18,
            nombre: "Guku SS1",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/gokuss1.png",
            aparicion: "2007-12-21",
            nivel: 'uno'
        },
        {
            iddragon: 20,
            nombre: "Guku SS3",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/gokuss3.png",
            aparicion: "2007-12-21",
            nivel: 'tres'
        },
        {
            iddragon: 21,
            nombre: "Piccolo",
            description: "Es hijo de Piccolo Daimacu, quien lo engendró poco antes de morir a manos de Goku, con el deseo de que tomara venganza algún día. Pero con el tiempo ese propósito queda en el pasado y Piccolo se hace mucho más fuerte y menos malvado, debido a que Majunia no lo es tanto.",
            img: "assets/img/dragon/piccolo.png",
            aparicion: "1994-10-21",
            nivel: 'cero'
        },
        {
            iddragon: 22,
            nombre: "Vegeta SS1",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/vegetass1.png",
            aparicion: "2007-12-21",
            nivel: 'uno'
        },
        {
            iddragon: 23,
            nombre: "Vegeta SS2",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/vegetass2.png",
            aparicion: "2007-12-21",
            nivel: 'dos'
        },
        {
            iddragon: 24,
            nombre: "Goku SS4",
            description: "Es un estado que solo pueden alcanzar los Saiyan con gran potencial latente y/o arduo entrenamiento al activar sus células S, así como los mestizos de esta misma raza guerrera o gente con la sangre de la misma lo pueden obtener.",
            img: "assets/img/dragon/gokuss4.png",
            aparicion: "2007-12-21",
            nivel: 'cuatro'
        }
    ];
    getHeroes() {
        return this.heroes;
    }
    getDragonBall() {
        return this.dragon;
    }
}