import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DiplomadoService {

public listas = [
    { id: 1, tipo: 'PENDIENTE' },
    { id: 2, tipo: 'INICIADO' },
    { id: 3, tipo: 'FINALIZADO' }
  ];

  public prueba = [
    {
      id_apertura: 1,
      mencion: 'Atención farnaceutica y farmacia clinica',
      ciclo: '2/2018-ii',
      pagina: '001',
      fecha: '23-12-2018',
      estado: 'PENDIENTE',
      id_diplo: {
        id_diplo: 1,
        section: 'Ciencias de la salud',
        asignatura: 'Farmacoterapia i',
        sicle: 'cf-720',
        creditos: 2.0
      }
    }
  ];
  public diplomados = [
    {
      id_diplo: 1,
      section: 'Ciencias de la salud',
      maestria: 'Atención farnaceutica y farmacia clinica',      
      asignatura: 'Farmacoterapia i',
      sicle: 'cf-720',
      creditos: 2
    },
    {
      id_diplo: 2,
      section: 'Ciencias de la salud',
      maestria: 'Atención al niño',      
      asignatura: 'Pacientes i',
      sicle: 'pp-120',
      creditos: 2
    }
  ];

  constructor() { }
}
