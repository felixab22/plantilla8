import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DiplomadoService,
  HeroesService,
  AlumnosService
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    DiplomadoService,
    HeroesService,
    AlumnosService
  ]
})
export class ServicesModule { }
